const baseUrl = "http://react-laravel-todo/api/tareas"
import axios from "axios"


const tarea = {}

tarea.list = async() => {
    const urlList = baseUrl
    const res = await axios.get(urlList)
        .then(response => {
            return response.data
        })
        .catch(error => {
            return error
        })
    return res
}

tarea.save = async(data) => {
    const urlSave = baseUrl
    const res = await axios.post(urlSave, data)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            return error;
        })
    return res;
}

tarea.delete = async(id) => {
    const urlDelete = baseUrl + '/' + id
    const res = await axios.delete(urlDelete)
        .then(response => {
            return response.data
        })
        .catch(error => {
            return error
        })
    return res;
}

export default tarea