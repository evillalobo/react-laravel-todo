import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import ListadoTareas from './ListadoTareas'
import CreateTarea from './CreateTarea'

import tareaService from '../services/Tarea'



function Main({_token}) {
    const [tareasList, setTareasList] = useState([])
    const [refreshKey, setRefreshKey] = useState(0)
    const [token, setToken] = useState()
    
    useEffect(
        () => {
            const fetchDataTareas = async () => {
                setToken(_token)

                const res = await tareaService.list()//enviar con el token

                setTareasList(res)
                
            }

            fetchDataTareas()
        }, [refreshKey]
    )

    const onAddSubmit = async (data) => {

        const res = await tareaService.save(data)
        
        setRefreshKey(refreshKey + 1)

    };

    const onDeleteTarea = async (id) => {
        var opcion = confirm("Estas seguro que deseas eliminar la tarea?");
        if (opcion === true) {
            const borrarTarea = async (id) => {
                const res = await tareaService.delete(id)
                setTareasList(tareasList.filter((tarea) => tarea.id !== id))

                console.log(res)
            }

            borrarTarea(id)
        } else {
            console.log("no elimina la tarea : " + id);
        }
    };

    return (
        <div className="container">
            <div className="py-5 text-center">
                <img className="d-block mx-auto mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                <h2>To-do app with React and Laravel</h2>
                <p className="lead">Aplicación hecha con Laravel y React</p>
            </div>

            <div className="row">
                <div className="col-md-8 order-md-1">
                    <h4 className="mb-3">Datos de la tarea</h4>
                    <CreateTarea onAddSubmit={onAddSubmit} />
                </div>
                <div className="col-md-4 order-md-2 mb-4">
                    <ListadoTareas tareasList={tareasList} onDeleteTarea={onDeleteTarea} />
                </div>

            </div>

            <footer className="my-5 pt-5 text-muted text-center text-small">
                <p className="mb-1">© 2017-2018 Company Name</p>
                <ul className="list-inline">
                    <li className="list-inline-item"><a href="#">Privacy</a></li>
                    <li className="list-inline-item"><a href="#">Terms</a></li>
                    <li className="list-inline-item"><a href="#">Support</a></li>
                </ul>
            </footer>
        </div>
    )
}

export default Main

if (document.getElementById('main')) {
    var data = document.getElementById('main').getAttribute('_token');
    ReactDOM.render(<Main _token={data}/>, document.getElementById('main'));
}