import React, { useState } from 'react'
import tareaService from '../services/Tarea'

function CreateTarea ({onAddSubmit}) {

    const [titulo, setTitulo] = useState('')
    const [descripcion, setDescripcion] = useState('')
    const [fecha, setFecha] = useState('')

    const data = {
        titulo, descripcion, fecha
    }

    const handleOnSumbit = (e) =>{
        e.preventDefault()

        onAddSubmit(data)

        setTitulo('')
        setDescripcion('')
        setFecha('')
    }
    
    return (
        <div>
            <form onSubmit={handleOnSumbit}>
                <div className="row">
                    <label htmlFor="tarea">Tarea</label>
                    <input type="text" className="form-control" id="tarea" placeholder="" required=""
                        value={titulo}
                        onChange={e => setTitulo(e.target.value)} />
                    <div className="invalid-feedback">
                        Una tarea valida es requerida
                        </div>
                </div>

                <div className="row">
                    <label htmlFor="descripcion">Descripción</label>
                    <textarea type="text" className="form-control" id="descripcion" placeholder="" required=""
                        value={descripcion}
                        onChange={e => setDescripcion(e.target.value)} />
                    <div className="invalid-feedback">
                        Descripcion valida es requerida
                        </div>
                </div>
                <div className="row">
                    <label htmlFor="lastName">Fecha</label>
                    <input type="date" className="form-control" id="fecha" placeholder="" required=""
                        value={fecha}
                        onChange={e => setFecha(e.target.value)} />
                    <div className="invalid-feedback">
                        Fecha valida es requerida
                        </div>
                </div>
                <hr className="mb-4"></hr>
                <input type="submit" className="btn btn-primary btn-lg btn-block" value="Crear tarea"/>
            </form>
        </div>
    )
}

export default CreateTarea
