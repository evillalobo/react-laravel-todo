
import React from 'react'
import Tarea from './Tarea'


function ListadoTareas({tareasList, onDeleteTarea}) {
    return (
        <div>
            <h4 className="d-flex justify-content-between align-items-center mb-3">
                <span className="text-muted">Tus Tareas</span>
                <span className="badge badge-secondary badge-pill">3</span>
            </h4>
            <ul className="list-group mb-3">
                {
                    tareasList.map(
                        (item) => {
                            return (
                                <Tarea tarea={item} key={item.id} onDeleteTarea={onDeleteTarea} />
                            )
                        }
                    )
                }
            </ul>
        </div>
    )
}

export default ListadoTareas
