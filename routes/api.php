<?php

use App\Http\Controllers\TareaController;
use App\Http\Controllers\TodoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->post('/login', [TodoController::class, 'getAccessToken']);

Route::get('/tareas', [TareaController::class, 'index'])->name('tareas.all');
Route::post('/tareas', [TareaController::class, 'store'])->name('tareas.store');
Route::middleware('auth:sanctum')->post('/tareas/post', [TareaController::class, 'store'])->name('tareas.store');
Route::get('/tareas/{tarea}', [TareaController::class, 'show'])->name('tareas.show');
Route::delete('/tareas/{id}', [TareaController::class, 'destroy'])->name('tareas.destroy');

